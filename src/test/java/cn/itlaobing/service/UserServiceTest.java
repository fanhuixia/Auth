package cn.itlaobing.service;
import cn.itlaobing.entity.UserEntity;
import cn.itlaobing.service.impl.UserServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
/**
 * Created by admin on 2017/11/2.
 */
public class UserServiceTest {
    /*要测试的接口*/
    private UserService userService=new UserServiceImpl();

    /*测试之前执行的方法*/
    @Before
    public void before(){
        userService=new UserServiceImpl();
    }
    @Test
    public void testAuthentication() {
        String account ="fhx_2012";
        String password="95120321";

        UserEntity userEntity=userService.authentication(account,password);
        /*判断测试用例是否通过(利用断言)*/
        /*要求uer不能是空的*/
        Assert.assertNotNull(userEntity);
        Assert.assertEquals(account,userEntity.getUseraccount());
        Assert.assertEquals(password,userEntity.getUserpwd());
        //Assert.assertNull(userEntity.getLogintime());


    }
}

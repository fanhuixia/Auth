<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>用户登录</title>
    <base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}">
    <!-- Error CSS -->
    <link href="assets/lib/css/login.css" rel="stylesheet" media="screen">

    <!-- Animate CSS -->
    <link href="assets/lib/css/animate.css" rel="stylesheet" media="screen">

    <!-- Font Awesome -->
    <link href="assets/lib/fonts/font-awesome.min.css" rel="stylesheet">
</head>
<body>
<form action="/auth/signin" method="post">
    <div id="box" class="animated bounceIn">
        <div id="top_header">
            <h5>
                    用&nbsp;户&nbsp;登&nbsp;录
            </h5>
        </div>
        <di id="inputs">
            <div class="form-control">
                <input type="text" name="account" placeholder="请输入用户名后手机号" >
                <i class="fa fa-envelope-o"></i>
            </div>
            <div class="form-control">
                <input type="password" name="pwd" placeholder="请输入密码" >
                <i class="fa fa-key"></i>
            </div>
            <input type="submit" value="登录">
        </di>

        <c:if test="${not empty msg}">
            <div style="color:brown;text-align: center" >
                    ${msg}
            </div>
        </c:if>
    </div>
</form>
</body>
</html>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>订单详情</title>
    <%@include file="../layout/_head_css.jsp"%>
</head>
<body>
<%@include file="../layout/menu.jsp"%>
<!-- Dashboard Wrapper Start -->
<div class="dashboard-wrapper">
    <%@include file="../layout/_head_header.jsp"%>


    <!-- Main Container Start -->
    <div class="main-container">

        <!-- Top Bar Starts -->
        <div class="top-bar clearfix">
            <div class="page-title">
                <h4><div class="fs1" aria-hidden="true" data-icon="&#xe0a0;"></div>Default Layout <a href="invoice.html" class="samll">Bonus Pages</a></h4>
            </div>
            <ul class="right-stats hidden-xs" id="mini-nav-right">
                <li class="reportrange btn btn-success">
                    <i class="fa fa-calendar"></i>
                    <span></span> <b class="caret"></b>
                </li>
                <li>
                    <a href="#" class="btn btn-info sb-open-right sb-close">
                        <div class="fs1" aria-hidden="true" data-icon="&#xe06a;"></div>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Top Bar Ends -->
        <!-- Container fluid Starts -->
        <div class="container-fluid">

            <!-- Spacer starts -->
            <div class="spacer-xs">
                <form  action="/orders/show"　method="post">
                    <table border="1" cellpadding="0" cellspacing="0" width="100%" style="text-align: center">
                        <tr >
                            <td colspan="9"><h5>订单列表</h5></td>
                        </tr>
                        <tr>
                            <td>订单编号</td>
                            <td>下单时间</td>
                            <td>总金额</td>
                            <td>订单状态</td>
                            <td>来源</td>
                            <td>收货人姓名</td>
                            <td>收货人电话</td>
                            <td>收货人地址</td>
                            <td>操作</td>
                        </tr>
                        <c:if test="${empty orderList }">
                            <h1>暂时没有数据</h1>
                        </c:if>
                        <c:if test="${not empty orderList }">
                            <c:forEach var="order" items="${orderList}">
                                <tr>
                                    <td>${order.orderid}</td>
                                    <td>
                                        <fmt:formatDate value="${order.ordertime}" pattern="yyyy-MM-dd " />
                                    </td>
                                    <td>
                                        <fmt:formatNumber value="${order.total}" pattern="0.00" />
                                    </td>
                                    <td>
                                            ${order.orderStatus.text}
                                    </td>
                                    <td>${order.orderFrom.text}</td>
                                    <td>${order.acceptperson}</td>
                                    <td>${order.acceptnumber}</td>
                                    <td>${order.acceptaddr}</td>
                                    <td>
                                        <c:if test="${order.orderStatus=='NO_PAYMENT'}">
                                            <button>支付</button>
                                            <button>取消</button>
                                        </c:if>
                                    </td>
                                </tr>
                            </c:forEach>
                        </c:if>
                    </table>
                </form>
            </div>
            <!-- Spacer ends -->
        </div>
        <!-- Container fluid ends -->

    </div>
    <!-- Main Container Start -->

    <!-- Footer Start -->
    <%@include file="../layout/_footer.jsp"%>
    <!-- Footer end -->

</div>
<!-- Dashboard Wrapper End -->
<%@include file="../layout/_footer.js.jsp"%>
</body>
</html>
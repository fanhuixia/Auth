<base href="${pageContext.request.scheme}://${pageContext.request.serverName}:${pageContext.request.serverPort}${pageContext.request.contextPath}">
<meta name="description" content="Cloud Admin Panel" />
<meta name="keywords" content="Admin, Dashboard, Bootstrap3, Sass, transform, CSS3, HTML5, Web design, UI Design, Responsive Dashboard, Responsive Admin, Admin Theme, Best Admin UI, Bootstrap Theme, Bootstrap, Light weight Admin Dashboard,Light weight, Light weight Admin, Light weight Dashboard" />
<meta name="author" content="Bootstrap Gallery" />
<link rel="shortcut icon" href="img/favicon.ico">
<!-- Bootstrap CSS -->
<link href="assets/lib/css/bootstrap.min.css" rel="stylesheet" media="screen">
<!-- Animate CSS -->
<link href="assets/lib/css/animate.css" rel="stylesheet" media="screen">
<!-- date range -->
<link rel="stylesheet" type="text/css" href="assets/lib/css/daterange.css">
<!-- Main CSS -->
<link href="assets/lib/css/main.css" rel="stylesheet" media="screen">
<!-- Slidebar CSS -->
<link rel="stylesheet" type="assets/lib/text/css" href="assets/lib/css/slidebars.css">
<!-- Font Awesome -->
<link href="assets/lib/fonts/font-awesome.min.css" rel="stylesheet">
<!-- Metrize Fonts -->
<link href="assets/lib/fonts/metrize.css" rel="stylesheet">
<!-- HTML5 shiv and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
<script src="assets/lib/js/html5shiv.js"></script>
<script src="assets/lib/js/respond.min.js"></script>
<![endif]-->
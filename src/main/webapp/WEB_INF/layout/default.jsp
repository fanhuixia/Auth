<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
		<title>订单详情</title>
        <%@include file="_head_css.jsp"%>
	</head>
	<body>
     <%@include file="menu.jsp"%>
     <!-- Dashboard Wrapper Start -->
     <div class="dashboard-wrapper">
         <%@include file="_head_header.jsp"%>


         <!-- Main Container Start -->
         <div class="main-container">

             <!-- Top Bar Starts -->
             <div class="top-bar clearfix">
                 <div class="page-title">
                     <h4><div class="fs1" aria-hidden="true" data-icon="&#xe0a0;"></div>Default Layout <a href="invoice.html" class="samll">Bonus Pages</a></h4>
                 </div>
                 <ul class="right-stats hidden-xs" id="mini-nav-right">
                     <li class="reportrange btn btn-success">
                         <i class="fa fa-calendar"></i>
                         <span></span> <b class="caret"></b>
                     </li>
                     <li>
                         <a href="#" class="btn btn-info sb-open-right sb-close">
                             <div class="fs1" aria-hidden="true" data-icon="&#xe06a;"></div>
                         </a>
                     </li>
                 </ul>
             </div>
             <!-- Top Bar Ends -->
             <!-- Container fluid Starts -->
             <div class="container-fluid">

                 <!-- Spacer starts -->
                 <div class="spacer-xs">

                 </div>
                 <!-- Spacer ends -->
             </div>
             <!-- Container fluid ends -->

         </div>
         <!-- Main Container Start -->

         <!-- Footer Start -->
         <%@include file="_footer.jsp"%>
         <!-- Footer end -->

     </div>
     <!-- Dashboard Wrapper End -->
     <%@include file="_footer.js.jsp"%>
	</body>
</html>
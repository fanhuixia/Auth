<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="assets/lib/js/jquery.js"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="assets/lib/js/bootstrap.min.js"></script>

<!-- Animated Right Sidebar -->
<script src="assets/lib/js/slidebars.js"></script>

<!-- Date Range -->
<script src="assets/lib/js/daterange/moment.js"></script>
<script src="assets/lib/js/daterange/daterangepicker.js"></script>

<!-- Custom JS -->
<script src="assets/lib/js/custom.js"></script>
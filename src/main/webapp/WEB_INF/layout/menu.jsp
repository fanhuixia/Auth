<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<!-- Left sidebar start -->
<aside id="sidebar">

    <!-- Logo starts -->
    <a href="#" class="logo">
        <img src="assets/lib/img/logo.png" alt="logo">
    </a>
    <!-- Logo ends -->

    <!-- Menu start -->
    <div id='menu'>
        <ul>
            <li>
                <a href='index.html'>
                    <div class="fs1" aria-hidden="true" data-icon="&#xe007;"></div>
                    <span>仪表板</span>
                </a>
            </li>
            <li>
                <a href='graphs.html'>
                    <div class="fs1" aria-hidden="true" data-icon="&#xe0f8;"></div>
                    <span>图表</span>
                </a>
            </li>
            <li class='has-sub '>
                <a href='#'>
                    <div class="fs1" aria-hidden="true" data-icon="&#xe052;"></div>
                    <span>UI元素</span>
                </a>
                <ul>
                    <li>
                        <a href='panels.html'>
                            <span>画板和列表组</span>
                        </a>
                    </li>
                    <li>
                        <a href='buttons.html'>
                            <span>纽扣</span>
                        </a>
                    </li>
                    <li>
                        <a href='grid.html'>
                            <span>格</span>
                        </a>
                    </li>
                    <li>
                        <a href='ui-elements.html'>
                            <span>更多</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li class='has-sub'>
                <a href='#'>
                    <div class="fs1" aria-hidden="true" data-icon="&#xe0ab;"></div>
                    <span>形式</span>
                </a>
                <ul>
                    <li>
                        <a href='form.html'>
                            <span>表单元素</span>
                        </a>
                    </li>
                    <li>
                        <a href='form-wizard.html'>
                            <span>表单向导</span>
                        </a>
                    </li>
                    <li>
                        <a href="editor.html">
                            <span>编辑器</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href='gallery.html'>
                    <div class="fs1" aria-hidden="true" data-icon="&#xe0e6;"></div>
                    <span>画廊</span>
                </a>
            </li>
            <li>
                <a href="tables.html">
                    <div class="fs1" aria-hidden="true" data-icon="&#xe0f2;"></div>
                    <span>表格</span>
                </a>
            </li>
            <li class='has-sub highlight active'>
                <a href='#'>
                    <div class="fs1" aria-hidden="true" data-icon="&#xe0a0;"></div>
                    <span>处理页</span>
                </a>
                <ul style="display: block">
                    <li>
                        <a href='invoice.html'>
                            <span>发票</span>
                        </a>
                    </li>
                    <li>
                        <a href='calendar.html'>
                            <span>日历</span>
                        </a>
                    </li>
                    <li>
                        <a href='login.html'>
                            <span>登录</span>
                        </a>
                    </li>
                    <li>
                        <a href='404.html'>
                            <span>404错误页</span>
                        </a>
                    </li>
                    <li>
                        <a href='default.jsp' class="select">
                            <span>默认页</span>
                        </a>
                    </li>
                </ul>
            </li>
            <li>
                <a href='maps.html'>
                    <div class="fs1" aria-hidden="true" data-icon="&#xe089;"></div>
                    <span>矢量地图</span>
                </a>
            </li>
            <li>
                <a href='notifications.html'>
                    <div class="fs1" aria-hidden="true" data-icon="&#xe0e9;"></div>
                    <span>通知</span>
                </a>
            </li>
            <li>
                <a href='typography.html'>
                    <div class="fs1" aria-hidden="true" data-icon="&#xe019;"></div>
                    <span>样式</span>
                </a>
            </li>
        </ul>
    </div>
    <!-- Menu End -->

</aside>
<!-- Left sidebar end -->

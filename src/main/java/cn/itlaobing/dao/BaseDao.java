package cn.itlaobing.dao;
import com.alibaba.druid.pool.DruidDataSource;

import javax.sql.DataSource;

/**
 * Created by admin on 2017/11/2.
 */
public class BaseDao {
    private final static String DB_URL ="jdbc:mysql://localhost:3306/orderdb?characterEncoding=utf8";
    private final static String DB_USER = "root";
    private final static String DB_PASSWORD = "root";
    private final static String DB_DRIVER_CLASS_NAME = "com.mysql.jdbc.Driver";
    private DataSource dataSource = null;

    /*创建数据库连接*/
    protected DataSource getDataSource() {
        if (dataSource != null) {
            return dataSource;
        }
        DruidDataSource dataSource = new DruidDataSource();
        dataSource.setUrl(DB_URL);
        dataSource.setUsername(DB_USER);
        dataSource.setPassword(DB_PASSWORD);
        dataSource.setDriverClassName(DB_DRIVER_CLASS_NAME);
        this.dataSource=dataSource;
        return dataSource;
    }
}



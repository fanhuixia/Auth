package cn.itlaobing.dao;
import cn.itlaobing.entity.UserEntity;
import org.apache.commons.dbutils.ResultSetHandler;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by admin on 2017/11/2.
 */
public class UserResultHandler implements ResultSetHandler<UserEntity> {
    public UserEntity handle(ResultSet resultSet) throws SQLException {
        if(resultSet.next()){
            UserEntity user=new UserEntity();
            user.setUseraccount(resultSet.getString("useraccount"));
            user.setUserpwd(resultSet.getString("userpwd"));
            return user;
        }
        return null;

    }
}

package cn.itlaobing.dao;
import cn.itlaobing.entity.UserEntity;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.BeanHandler;
import java.sql.SQLException;
/**
 * Created by admin on 2017/11/2.
 */
public class UserDao  extends BaseDao{
    public UserEntity findByAccount(String account) throws SQLException {
        QueryRunner qr=new QueryRunner(getDataSource());
        String sql="select * from users where useraccount=? or phonenumber=?";
        BeanHandler<UserEntity> userBeanHandler=new BeanHandler<UserEntity>(UserEntity.class);
        UserEntity user=qr.query(sql, userBeanHandler,account,account);
        return user;
    }
    /**
     * 更新登录时间为当前的时间
     */
    public void updateLastTime(Long userid) throws SQLException {
        QueryRunner qr=new QueryRunner(getDataSource());
        String sql="update users set logintime=now() where userid=?";
        qr.update(sql,userid);
    }
}


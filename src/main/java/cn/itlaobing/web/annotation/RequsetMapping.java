package cn.itlaobing.web.annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Method;

@Retention(RetentionPolicy.RUNTIME)//运行的时候可用
@Target(ElementType.METHOD)  //只修饰方法
public @interface RequsetMapping {
    String value() default "";
}

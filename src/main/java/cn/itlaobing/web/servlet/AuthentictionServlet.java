package cn.itlaobing.web.servlet;
import cn.itlaobing.entity.UserEntity;
import cn.itlaobing.service.UserService;
import cn.itlaobing.service.impl.UserServiceImpl;
import cn.itlaobing.web.annotation.RequsetMapping;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
/**
 * Created by admin on 2017/11/6.
 */
@WebServlet("/auth/*")
public class AuthentictionServlet extends BaseServlet{
    @RequsetMapping("/signin")
    public void signin(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String account = request.getParameter("account");
        request.setCharacterEncoding("utf-8");
        response.setCharacterEncoding("utf-8");
        String pwd=request.getParameter("pwd");
        UserService userService=new UserServiceImpl();
        UserEntity user=userService.authentication(account,pwd);
        if(user==null){
            request.setAttribute("msg","用户名或者密码错误！");
            request.getRequestDispatcher("/signin.jsp").forward(request,response);
            return;
        }
        //登录成功后,将用户放入到session中，使用重定向
        request.getSession().setAttribute(Constants.CURRENT_USER,user);
        response.sendRedirect(request.getContextPath()+"/orders/show");
    }
}

package cn.itlaobing.web.servlet;
import cn.itlaobing.dao.OrderDao;
import cn.itlaobing.entity.OrderEntity;
import cn.itlaobing.entity.UserEntity;
import cn.itlaobing.web.annotation.RequsetMapping;
import com.alibaba.fastjson.JSON;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by admin on 2017/11/6.
 */
@WebServlet("/orders/*")
public class OrderServlet extends BaseServlet {
  @RequsetMapping("/show")
    public void show(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        OrderDao orderDao=new OrderDao();
        UserEntity user=(UserEntity) request.getSession().getAttribute(Constants.CURRENT_USER);
        try {
            List<OrderEntity> orderList=orderDao.selectByOrderid((user.getUserid()));
            request.setAttribute("orderList",orderList);
            toView(request,response,"/show");
            return;
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void payMoney(HttpServletRequest request,HttpServletResponse response){
        String orderid=request.getParameter("orderid");
        OrderDao orderDao=new OrderDao();
        UserEntity userEntity=(UserEntity) request.getSession().getAttribute(Constants.CURRENT_USER);
        try {
            OrderEntity orderEntity=orderDao.pay(orderid);
            response.setContentType("application/json;charset=utf-8");
            if (orderEntity==null){
                response.getWriter().write("");
                return;
            }
            Map<String,Object> map=new HashMap<>();
            map.put("orderId",orderEntity.getOrderid());
            map.put("orderStatus",orderEntity.getOrderStatus().getText());
            response.getWriter().write(JSON.toJSONString(map));
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}


package cn.itlaobing.service.impl;

import cn.itlaobing.entity.UserEntity;
import cn.itlaobing.service.UserService;
import cn.itlaobing.dao.UserDao;

import java.sql.SQLException;

/**
 * Created by admin on 2017/11/2.
 */

public  class UserServiceImpl implements UserService {
   @Override
    public UserEntity authentication(String account, String password) {
        UserDao userDao=new UserDao();
        try {
            UserEntity userEntity=userDao.findByAccount(account);
            if(userEntity==null){
                return userEntity;
            }
            if(!userEntity.getUserpwd().equals(password)){
                return null;
            }
            return userEntity;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}

package cn.itlaobing.service;
import cn.itlaobing.entity.UserEntity;

/**
 * Created by admin on 2017/11/2.
 */
public interface UserService {

    public UserEntity authentication(String account, String password);
}

package cn.itlaobing.entity;

/**
 * Created by admin on 2017/11/6.
 */
public enum  OrderFrom {
    //   网站/手机app/微信
    WEB_SIDE("网站"),
    APP("手机app"),
    WEICHAT("微信");
    private String text;
    private OrderFrom(String text){
        this.text=text;
    }
    public String getText(){
        return text;
    }

}

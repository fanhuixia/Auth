package cn.itlaobing.entity;

import javax.xml.crypto.Data;
import java.io.Serializable;

/**
 * Created by admin on 2017/11/2.
 */
public class UserEntity implements Serializable{
    private String useraccount;
    private String userpwd;
    private  Long userid;
    private String  logintime;
    private  String username;
    private  String phonenumber;

    public Long getUserid() {
        return userid;
    }

    public void setUserid(Long userid) {
        this.userid = userid;
    }

    public String getLogintime() {
        return logintime;
    }

    public void setLogintime(String logintime) {
        this.logintime = logintime;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getUseraccount() {
        return useraccount;
    }
    public void setUseraccount(String useraccount) {
        this.useraccount = useraccount;
    }

    public String getUserpwd() {
        return userpwd;
    }

    public void setUserpwd(String userpwd) {
        this.userpwd = userpwd;
    }

}


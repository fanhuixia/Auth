package cn.itlaobing.entity;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by admin on 2017/11/6.
 */
public class OrderEntity implements Serializable{
    private  long orderid;
    private Date ordertime;
    private String userid;
    private Double total;
    private OrderStatus orderStatus=OrderStatus.NO_PAYMENT;
    private OrderFrom orderFrom=OrderFrom.WEB_SIDE;
    private String acceptperson;
    private String acceptnumber;
    private String   acceptaddr;

    public long getOrderid() {
        return orderid;
    }

    public void setOrderid(long orderid) {
        this.orderid = orderid;
    }

    public Date getOrdertime() {
        return ordertime;
    }

    public void setOrdertime(Date ordertime) {
        this.ordertime = ordertime;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }

    public OrderFrom getOrderFrom() {
        return orderFrom;
    }

    public void setOrderFrom(OrderFrom orderFrom) {
        this.orderFrom = orderFrom;
    }

    public String getAcceptperson() {
        return acceptperson;
    }

    public void setAcceptperson(String acceptperson) {
        this.acceptperson = acceptperson;
    }

    public String getAcceptnumber() {
        return acceptnumber;
    }

    public void setAcceptnumber(String acceptnumber) {
        this.acceptnumber = acceptnumber;
    }

    public String getAcceptaddr() {
        return acceptaddr;
    }

    public void setAcceptaddr(String acceptaddr) {
        this.acceptaddr = acceptaddr;
    }
}

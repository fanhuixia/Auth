package cn.itlaobing.entity;
/**
 * Created by admin on 2017/11/6.
 */
public enum  OrderStatus {
    NO_PAYMENT("未支付"),
    ALLOCATE("正在配货"),
    OUT_OF_STORE("已出库"),
    SENDING("正在配送"),
    RECEIVED("已收货"),
    CANCLED("已取消");
    private String text;
    //枚举的构造方法必须是private
    private OrderStatus(String text){
        this.text=text;
    }
    public String getText(){
        return text;
    }
}
